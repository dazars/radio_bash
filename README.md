# Radio bash

Hola, a todos

Estaciones de radio por internet en terminal con mpv como reproductor


Script en bash para escuchar radio online

Dependencias:

    figlet

    mpv

    libnotify



## INSTALACIÓN

``` bash
git clone https://gitlab.com/dazars/radio_bash/

cd radio_bash

makepkg

makepkg -si
```

Y ejecutas con el comando:

radio_bash

### AGREGAR ESTACIONES DE RADIO

El archivo "$HOME/.config/bashradio/radios" contiene el nombre y la url de cada estacion de radio... Aqui puedes agregar las que tu deses bajo el  formato:

*Nombre de la estacion*

*http://ruta_hacia_la_estacion*

Según se muestra en ese archivo
